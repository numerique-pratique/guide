---
author: Florent Déclas
title: 1 - Faisons le point
tags:
  - équipement
  - organisation
  - outils
---

## **a. Êtes-vous bien équipé ?**

### 🏡 Personnellement

Perdre du temps à l’allumage de votre PC et à l’ouverture des fichiers n’est pas normal ! **Votre PC doit être réactif !** Sinon, cela signifie que vous êtes mal équipé. Pas question d’utiliser les outils numériques avec un PC qui vous fait attendre ! Vous êtes un·e professionnel·le de l’éducation et vous méritez le matériel adéquat !

L’administration nous verse [150€ net par an pour s’équiper](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042614360) (au mois de janvier). En ma qualité de fonctionnaire à l'éthique irréprochable, je ne commenterai pas ce montant colossal. Passons aux solutions.

Tout d’abord, **oublions l’idée selon laquelle la performance est nécessairement hors de prix**. Cette analogie n'est qu’une création marketing.

Dans la [fiche pratique n°1]((../02-fiches-pratiques/FP1.md)), vous trouverez des explications pour choisir votre PC en sachant décoder les fiches techniques des magasins. Je vous indiquerai également comment **s’équiper de manière écologique et économique** grâce au reconditionnement. 
Si votre PC vous donne entière satisfaction, peut-être désirerez-vous gagner en productivité et en confort. La [fiche pratique n°2](../02-fiches-pratiques/FP2.md) vous proposera une première solution avec le passage au **double écran**.

Une fois le matériel choisi, comme n’importe quel outil, il faudra **l’entretenir**. Pour cela, reportez vous à la [fiche pratique n°3]((../02-fiches-pratiques/FP3.md)) : **hygiène numérique**.

### 🏫 À l'école

Disposer d’outils numériques, c’est bien. Savoir s’en servir, c’est mieux. Nombreuses sont les écoles équipées sans le souci de savoir si les collègues sont en mesure d’en profiter pleinement.

La [fiche pratique n°4]((../02-fiches-pratiques/FP4.md)) vous donnera quelques pistes en vue d’équiper une classe ou une école. **Équiper une classe n’est pas seulement un achat**. La prise en compte des modalités de configuration, de mise à jour, d’entretien et de stockage du matériel est indispensable avant tout achat ! La formation des collègues est également à anticiper.

La [fiche pratique n°5]((../02-fiches-pratiques/FP5.md)) s’attarde sur l’utilisation du **vidéoprojecteur** et quelques-unes de ses fonctionnalités utiles au quotidien.

Enfin, la [fiche pratique n°6]((../02-fiches-pratiques/FP6.md)) concerne un outil que je trouve indispensable : le **visualiseur**.

## **b. Êtes-vous bien organisé ?**

Au fil des années, **ressources et liens s’accumulent**. Certains d’entre vous aiment vivre dangereusement sans aucune sauvegarde de leurs données. D’autres, à la manière des écureuils et leurs provisions pour l’hiver, dispatchent leurs ressources sur de nombreuses clés USB ou disques durs externes et finissent par s’y perdre. 
Si l’une de ces descriptions vous correspond et que vous souhaitez mettre de l’ordre dans vos affaires numériques, pensez à **sauvegarder et synchroniser vos données.**

En effet, depuis quelques années, en tant qu’agent du ministère de l’Education Nationale, vous disposez d’un **stockage en ligne (“cloud”) de 100 Go** (l’équivalent de 10 000 photos HD de notre ministre 💕 !).

Cependant, le cloud, c’est **bien plus qu’une simple sauvegarde en ligne !** C’est pourquoi je vous propose 5 fiches pratiques à ce sujet : 

- Ouvrir son espace de stockage [(n°7)](../02-fiches-pratiques/FP7.md).
- Synchroniser toutes ses machines pour s’affranchir des clés USB / disques durs externes [(n°8)](../02-fiches-pratiques/FP8.md).
- Partager facilement des fichiers [(n°9)](../02-fiches-pratiques/FP9.md).
- Créer un espace de dépôt pour recevoir des fichiers de sources extérieures [(n°10)](../02-fiches-pratiques/FP10.md).
- Collaborer, créer, éditer [(n°11)](../02-fiches-pratiques/FP11.md).

## **c. Garnir sa boite à outils numériques**

Concernant les outils numériques, j’apprécie l’image de la boite à outils ou d’un couteau suisse à personnaliser. Avec quels outils pouvons-nous nous équiper ? La réponse dépend de chacun, de ses besoins, du matériel à disposition et de sa façon d’enseigner. Voyons ensemble quelques pistes ...

### 🧰 Des outils de production

#### Microsoft Office (Word, Excel, PowerPoint...)

Suite bureautique complète développée par Microsoft. Elle n’est, je pense, plus à présenter.
Actuellement deux options s’offrent à vous :  

- **Microsoft famille 2024** à 149€ pour un achat définitif limité à un seul ordinateur.

- **Office 365**, avec un stockage en ligne et liée à un abonnement mensuel ou annuel. Ce modèle économique vous enchaîne à vie et représente un coût énorme pour un outil utile dans le cadre de votre travail et non financé par votre employeur (depuis 2021, les solutions liées à un stockage en ligne de Microsoft ou Google sont proscrites au sein des ministères français [¹](https://acteurspublics.fr/upload/media/default/0001/36/acf32455f9b92bab52878ee1c8d83882684df1cc.pdf) [²](https://questions.assemblee-nationale.fr/q16/16-971QE.htm)).

Microsoft communique également sur une solution Education gratuite pour les enseignants mais je ne sais pas si nous y sommes réellement éligibles. Quoiqu’il en soit, il existe d’autres suites bureautiques performantes sans pour autant vous enchainer à un abonnement ou vous cloisonner dans un écosystème particulier.

#### LibreOffice (Writer, Calc, Impress...)

Suite bureautique complète et **gratuite**. Elle est développée par la Document Foundation ; une organisation à **but non lucratif**. Elle évolue constamment et ses possibilités sont aujourd’hui largement à la hauteur de la suite Microsoft Office pour nos usages de profs.
Évidemment, passer de l’une à l’autre demande quelques changements d’habitudes mais les intentions ne sont clairement pas les mêmes. D’un côté ; un géant du numérique qui fait tout pour conserver son hégémonie et augmenter sa rentabilité (Microsoft). De l’autre ; une organisation à but non lucratif dont la mission est de diffuser une **suite bureautique gratuite, ouverte et centrée sur l’utilisateur.**

#### OpenOffice (Writer, Calc, Impress...)
Peut-être avez-vous également entendu parlé d’OpenOffice ?
Pour faire court, OpenOffice est l’ancêtre de LibreOffice. Ces deux suites sont « open source » ce qui signifie que leur code source est accessible à tous et réutilisable. C’est ainsi que LibreOffice est né à partir d’OpenOffice (suite à une histoire de rachat et de désaccords que je ne développerai pas ici) et profite d’une communauté bien plus active qu’OpenOffice. Cela permet donc à LibreOffice d’évoluer plus rapidement et de bénéficier d’un meilleur support.

#### OnlyOffice

Connue surtout pour sa suite bureautique en ligne, Onlyoffice propose également une **version hors ligne gratuite et performante** [https://www.onlyoffice.com/download-desktop.aspx](https://www.onlyoffice.com/download-desktop.aspx) à installer sur sa machine (PC, smartphone et tablette).
L’interface d’OnlyOffice est très proche de celle de Microsoft Office et sa **compatibilité** avec ses formats de fichiers habituels (.docx, .pptx...) est plutôt bonne.
Parmi les outils classiques (traitement de texte, tableur et logiciel de présentation), OnlyOffice inclut également un outil de **création et d’édition de PDF**.
OnlyOffice propose également un ingénieux **système de “plugins”** vous permettant, par exemple, d’ajouter un outil de recherche d’images (comme Pixabay) pour les insérer facilement dans vos documents sans avoir à passer par votre navigateur internet.

#### À propos des formats
***.docx** (texte), **.xlsx** (tableur) et **.pptx** (présentation) sont les formats de fichiers privilégiés de Microsoft car ils sont conçus par Microsoft pour fonctionner de manière optimale avec leurs outils.* 
*Les formats **.odt** (texte), **.ods** (tableur) et **.odp** (présentation) sont des formats conçus pour être libres d’utilisation, standards et ouverts à n’importe quel logiciel. C’est pourquoi ce format est privilégié par LibreOffice. Cela assure un accès optimal aux données indépendamment du logiciel utilisé.*
*Microsoft Office, OnlyOffice et LibreOffice sont capables d’ouvrir tous ces formats de fichiers, mais, selon leur origine et leur complexité, la compatibilité peut ne pas être parfaite.*

#### Canva

Logiciel de **conception graphique en ligne**, Canva vous permettra de créer toutes sortes de supports visuels pédagogiques : documents, diaporamas, affiches, vidéos …

Canva met à disposition toutes ses fonctionnalités (modèles pré-conçus, outils IA, banque d’images, 100Go de stockage …) **gratuitement aux enseignants** à travers sa version « Education ». Pour cela, il suffit de s’inscrire à l’aide de son adresse mail académique sur [ce lien](https://www.canva.com/education/).

#### Polotno

Logiciel de **conception graphique en ligne libre** ! *(contrairement à Canva)*. Disponible via [Digidesign](https://ladigitale.dev/digidesign/) mais également la [forge des communs numériques éducatifs](https://polotno-studio-665dba.forge.apps.education.fr/).


### ⚡ Des générateurs

Avant de vous lancer dans une tâche complexe et chronophage, cherchez sur le net si une solution existe. **Internet regorge de générateurs** qui s’avèrent très utiles pour gagner du temps : modèles d’écriture, activités ludiques (mots croisés, dominos, rébus ...), patrons de boites, représentations mathématiques (fractions, numération, horloges ...), frises chronologiques, tests de fluence, de calcul mental, de conjugaison, ...
La [fiche pratique n°25](../02-fiches-pratiques/FP24.md) vous donnera plusieurs adresses de générateurs à ajouter dans votre boite à outils.

### ✅ Apps.education.fr

Site proposé par notre **ministère**. Leur meilleure création à mon goût (après « iProf », évidemment 😋).
“Apps” regroupe plusieurs outils dont certains vous feront gagner du temps au quotidien. Je pense notamment à :

- « **Nuage** » : votre stockage en ligne (cloud) de 100 Go ! ([fiche pratique n°7](../02-fiches-pratiques/FP7.md)).
- « **Evento** » : pour trouver la date idéale d’un événement à plusieurs ([fiche pratique n°31](../02-fiches-pratiques/FP31-1.md)).
- « **Sondage** » : pour proposer votre “Doctolib perso” à destination des parents et faciliter la prise de rendez-vous ! ([fiche pratique n°30](../02-fiches-pratiques/FP29.md)).


**A savoir :** inutile de créer un compte puisque vous pouvez vous identifier directement avec vos **identifiants académiques**. D’autre part, l’adresse mail de l’école permet également d’agir comme un compte à part : idéal pour créer un espace de collaboration et profiter de 100Go de stockage commun !

### 🔵 La Digitale

« Couteau suisse » par excellence proposant différents outils **gratuits** à utiliser **directement dans votre navigateur** et **sans inscription**. Chaque outil est indépendant. Avec La Digitale, vous pouvez :

- **animer vos cours** (buzzer connecté, compte à rebours, sondages, questionnaires, …),
- **partager des contenus** (bouquet de liens, codes QR, partager une vidéo ou une page internet sans distraction, partager du contenu sans fil…).
- **créer ou modifier des contenus multimédias** (créer des flashcards, des cartes mentales, des BD, des diaporamas, modifier des PDF, couper vos enregistrements audios/vidéos,  …).
- **créer des activités collaboratives** (tableau blanc, tableur, nuage de mots, remue-méninges, …),

La Digitale propose également des logiciels à télécharger et installer sur son ordinateur pour télécharger des vidéos issues du net, créer un dictionnaire multimédia, réaliser des mixages audio, créer et lire du contenu H5P (contenu interactif).

### ⚙️ Des exerciseurs

Pour aider vos élèves à **développer des automatismes**, rien de mieux que les “exerciseurs”. Le retour (feedback) immédiat proposé est un très bon levier d’apprentissage. De plus, l’absence du regard des camarades et/ou de l’enseignant sur les erreurs commises est également rassurante pour certains enfants.

D’excellents sites permettent aux élèves de **s’entrainer en ligne, gratuitement et facilement**. Vous en trouverez plusieurs dans la 3e partie : “Naviguons ensemble”. D’autre part, il s’utilisent, pour la plupart, **directement dans un navigateur internet**. Cela permet de fonctionner sur n’importe quel terminal (PC, tablette) et système d’exploitation (Android, iOS, macOS, Linux, Windows).

### 👩‍🏫 Des outils de modélisation

Aujourd’hui, rares sont les classes sans vidéoprojecteur. Cela permet de profiter de **nouvelles possibilités de modélisation** ou de faciliter cette phase : monnaie, horloge, fractions, numération, lignes graduées, sciences, frises chronologiques ... 

Je ne parle évidemment pas de remplacer la manipulation réelle de vos élèves mais bien de **faciliter ou améliorer vos phases collectives**.
Pour cela, j’ai compilé quelques outils bien utiles dans la 3e partie : “Naviguons ensemble”. 

!!! abstract "Résumons"

    Avant de passer à l’action, récapitulons les outils à glisser dans votre boite à outils numériques : 
    
      * des outils de production, 
      * des générateurs, 
      * Apps.education.fr,
      * La Digitale,
      * des exerciseurs,
      * des outils de modélisation.
    
    Évidemment, cette liste n’est pas exhaustive. Vous pourrez la compléter et l’affiner selon vos besoins et vos découvertes tout au long de cet ouvrage ou de vos pérégrinations internautiques.