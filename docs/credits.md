---
author: Florent Déclas
title: 👏 Crédits
---

![brigit_et_komit](https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/brigit_et_komit_transparent.png "brigit et komit"){ width=10% }

Ce site est hébergé par la [forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/).

![AEIF](assets/images/logo_aeif_300.png){ width=10% }

Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.  

Merci aux auteurs du [tutoriel de création de site web sur la forge](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/). 

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){target="_blank"}, et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"} pour la partie Python nécessaire pour les QCM (fonction non utilisée dans ce site).