---
author: Florent Déclas
title: 4 - Changer les choses ...
tags:
  - libre
  - numérique
---

!!! warning "Précision"

    Dans le guide publié initialement sur Classeadeux.fr, la dernière partie s'intitule "Au bistrot". Celle-ci contient plusieurs paragraphes très subjectifs qui n'ont, à mon sens, pas leur place ici. Si cela vous intéresse tout de même de lire l'avis d'un des 8 milliards d'êtres humains au sujet du numérique éducatif (et pas seulement), rdv [ici](https://classeadeux.fr/wp-content/uploads/2024/11/Guide-Le-numerique-pratique.pdf). En attendant, restons sur du concret...

## Changer les choses ...

**N’attendons pas** que le changement viennent des acteurs actuellement dominants du secteur numérique. **Tant qu’ils rempliront leurs objectifs, rien ne changera**. Les réseaux sociaux et les hébergeurs de contenu vous pousseront toujours à plus de partages et de temps d’attention. Les vendeurs de matériel trouveront toujours l’innovation qui changera votre vie. Les semeurs d’idées néfastes débusqueront toujours les meilleurs canaux pour être relayés et gagner en popularité. Les courtiers en données (“data brokers”) auront toujours une piste pour suivre et monétiser votre vie privée.

Malgré tout, j’ai une **bonne nouvelle** ! Une manière alternative de vivre et de travailler avec les outils numériques existe. Celle-ci est déjà bien en place, elle est riche et offre un horizon encourageant.

### 💡 S'éclairer

Dans un premier temps, si l’aspect “**culture numérique**” vous parait flou, je vous conseille fortement une promenade sur le site personnel de [Louis Derrac](https://louisderrac.com/) intitulé [« Comprendre le numérique pour pouvoir le critiquer et le transformer »](https://louisderrac.com/). C’est l’endroit idéal pour se former, en tant que citoyen, à la chose numérique (enjeux, histoire, transformation, limites ...). Louis propose notamment un modèle très intéressant pour guider nos décisions concernant le numérique :

![numerique-acceptable](numacceptable.png){width=30%; : .center}

Le site [Alt IMPACT](https://altimpact.fr/) de l’ADEME est également très intéressant et traite de manière concrète la question d’un numérique responsable.

### 🔧 S'outiller autrement

**Il ne s’agit pas de tout changer du jour au lendemain !** Des habitudes bien ancrées sont toujours difficiles à modifier surtout si l’on ne se sent pas à l’aise dans le domaine. Pas de panique, pas de jugement ! Mon objectif est de vous présenter des pistes, des chemins alternatifs. A vous de les emprunter ou non, à votre rythme, lorsque vous vous sentirez prêt·e et à condition de les trouver pertinents !

![outils](outils.png){ .center }

Du côté des **ordinateurs**, je vous ai déjà parlé du reconditionnement (fiche pratique n°1) pour réduire les déchets et du passage à Linux (fiche pratique n°38) pour prolonger la durée de vie de votre machine et s’affranchir de Windows.

Du côté des **smartphones**, le reconditionnement existe également. D’ailleurs, certaines initiatives vont bien au-delà d’un « simple » reconditionnement en visant également des objectifs **sociaux**. Je pense notamment aux [**Ateliers du Bocage**](https://boutique.ateliers-du-bocage.fr/), une coopérative d'utilité sociale et environnementale, membre du mouvement Emmaüs.
Une autre alternative existe : la location. C’est la solution proposée par « [**Commown**](https://commown.coop/) », une coopérative qui vise à transformer les outils technologiques en **bien commun**. La location inclut de multiples services (prise en charge des pannes, assistance, remplacement de la batterie…) afin de rendre le matériel **le plus durable possible**.

Concernant les **services web**, les géants du numériques ont réussi à s’imposer comme une évidence et façonnent lentement mais sûrement nos usages selon leurs standards et dans leur intérêt. Pour vos mails, votre stockage en ligne personnel, vos échanges, vos outils de collaboration et de production, des **alternatives éthiques** existent et fonctionnent aussi bien. Parmi elles :

* **L’association Framasoft**, qui, entre autres, développe et héberge des [**services libres gratuits**](https://degooglisons-internet.org/fr/) répondant à la plupart des besoins (visioconférence, sondage, planification d’évènement, outils de collaboration ...). 

* Les [**CHATONS**](https://www.chatons.org), collectif d’hébergeurs alternatifs, transparents, ouverts, neutres et solidaires, proposent, comme Framasoft, des [**services libres décentralisés**](https://entraide.chatons.org/fr/).

* L'[**April**](https://www.april.org/) (Association pour la Promotion et la Recherche en Informatique Libre) propose également son bouquet de services libres sur le site [Chapril](https://www.chapril.org/-services-.html).

* Avec son offre à 12€ par an, [**Zaclys**](https://www.zaclys.com) vous permet de bénéficier de **services web libres** (mail, stockage en ligne, envoi de fichiers, album photo, carnet d’adresses …) sans qu’on ne touche à vos données pour les vendre, les analyser ou vous transformer en cible publicitaire.

Du côté **logiciels et applications**, votre **navigateur** cherche peut-être à en savoir plus que ce que vous [croyez](https://next.ink/brief_article/google-poursuivi-en-justice-pour-les-collectes-de-donnees-operees-par-chrome/) et ne s’occupe pas simplement d’afficher les pages internet. Si vous y êtes sensible, privilégiez l’utilisation de [**Firefox**](https://www.mozilla.org/fr/firefox/new/). Ne vous inquiétez pas pour vos favoris, vous pouvez les passer facilement d’un navigateur à l’autre ([tutoriel](https://support.mozilla.org/fr/kb/passer-de-chrome-a-firefox)).

**En tant qu’enseignant**, jetez également un coup d’œil aux **outils libres** proposés sur la [**forge des communs numériques éducatifs**](https://docs.forge.apps.education.fr/).

Votre **smartphone** peut également fonctionner et vous rendre les mêmes services sans l’Android proposé par Google à l’achat de votre téléphone. **D’autres systèmes existent** : [GrapheneOS](https://grapheneos.org/), [/e/OS](https://e.foundation/fr/e-os/), [CalyxOS](https://calyxos.org/), [IodeOS](https://iode.tech/fr/iodeos/), [LineageOS](https://lineageos.org/), … Certains vendeurs proposent des smartphones prêts à l’emploi pour ne pas vous embêter avec l’installation : [Ekimia](https://ekimia.fr/), [Commown](https://commown.coop/), [Murena](https://murena.com/fr/produits/smartphones/), [Iodé](https://shop.iode.tech/fr/)...
On pourrait même parler de votre forfait avec des alternatives comme [Telecoop](https://telecoop.fr/) ! Mais ... 

... je m’arrête là !

A ce stade, j’espère avoir démontré que **l’écosystème numérique ne tourne pas uniquement autour des géants** (Google et Microsoft pour ne citer qu’eux) **et des solutions du secteur privé**. Avouez que les mots *“coopérative”, “association”, “libre”, “éthique”*... sont rarement associés au numérique dans les médias. Pourtant, les alternatives qui vous respectent sont nombreuses, fonctionnent et fourmillent de solutions ! 

Pour continuer à creuser le sujet, je vous redirige vers une autre ressource de Louis Derrac : [https://alternatives-numeriques.fr/](https://alternatives-numeriques.fr/) ainsi que l’annuaire [Framalibre](https://framalibre.org/). 

Soyez curieux 😉.

### 🤝 Transmettre

En tant qu’enseignant, nous avons le pouvoir de semer des graines. 
Un **livre (libre!)** existe au sujet des outils numériques et de la possibilité de les mettre au service de notre autonomie, sans nous aliéner : **“Ada & Zangemann”**.

Ce livre s’adresse aussi bien aux enfants qu’aux adultes et permet de prendre conscience, à travers une véritable histoire et de belles illustrations, de l’enjeu collectif des outils numériques.

*«Zangemann est un inventeur mondialement connu et immensément riche. Enfants et adultes adorent ses fabuleuses inventions. Mais soudain, gros problème : les skateboards électroniques des enfants buggent et les glaces ont toutes le même parfum. Que se passe-t-il ? Ada, jeune fille curieuse, va découvrir comment Zangemann contrôle ses produits depuis son ordinateur en or. Avec ses amis, elle va bricoler des objets informatisés qui échappent aux décisions de Zangemann. Un livre pour les enfants et jeunes ados qui pourrait bien leur transmettre le plaisir de bricoler. Un livre sur l'informatique libre, la camaraderie et le rôle des filles pour une technique au service de l'autonomie. Un conte vivant et superbement illustré.»*

La version numérique d’Ada & Zangemann est à **prix libre**. La version imprimée est également à la vente pour 15 €. Vous pouvez aussi le lire en ligne [en cliquant ici](https://ada-lelivre.fr/az.html).

![ada](ada.png){width=30%; : .center}
