---
author: Florent Déclas
title: 🏡 Accueil
---

!!! info "Où suis-je ?"

    Ce site est la version interactive du guide ["Le numérique pratique pour les profs des écoles"](https://classeadeux.fr/le-numerique-pratique/).



![Couverture livre](images/Maquette-livre-petit.png){ align=left } 
## Qu'est-ce que c'est ?

Un **guide pratique** compilant ce qui, à mes yeux, peut être **utile dans l'exercice de vos fonctions de professeur des écoles**.
Que vous soyez technophile, technophobe ou entre les deux, les technologies numériques doivent être des **outils à votre service** et non l'inverse.

Ainsi, je vous propose :

* de faire le point au sujet de votre **boite à outils numériques**,
* **43 fiches pratiques**,
* **+ de 70 sites internet** à garder sous le coude,
* quelques graines d'**alternatives numériques** à semer chez vous, si le coeur vous en dit.

## Qui me parle ?

Florent, professeur des écoles en Côte d'Or et passionné par les outils numériques. Je resterai le plus **concret et simple** possible. Les solutions partagées ici résultent de **choix personnels** liés à mes expériences en classe ou en dehors. **Libre à chacun d’en explorer ou préférer d’autres.**

!!! warning "Remarque"
    
    Ce guide est **évolutif** ! Adressez-moi vos remarques, vos attentes ou vos questions, vos idées ou participez à son évolution [en cliquant ici](https://forge.apps.education.fr/numerique-pratique/guide/-/issues).
    
    Vous pouvez également le **copier**, l'**adapter**, le **modifier**, d'où sa présence sur la [forge des communs numériques éducatifs](https://forge.apps.education.fr/numerique-pratique/guide).