---
author: Florent Déclas
title: FP 2 - Travailler avec deux écrans
tags:
  - équipement
  - matériel
  - écran
---

## Pourquoi ?

Pour le **confort** et le **gain de temps** !
Plutôt que de basculer de fenêtres en fenêtres ou d’onglets en onglets sur un seul écran sachez qu’avec deux écrans vous bénéficierez de deux zones de travail. Vous passerez de l’un à l’autre de façon fluide puisque **l’écran secondaire vient en prolongement du principal**. Exemple d’utilisation : un écran pour votre espace de rédaction et l’autre pour vos recherches sur le net.

## Comment ?

Techniquement, cela n’est pas compliqué : **il suffit de brancher un deuxième écran à votre ordinateur**. Pour cela, identifier la prise vidéo de votre ordinateur (HDMI, mini HDMI, VGA, DVI, DisplayPort ...) et procurez-vous un écran en fonction.
Après branchement, l’affichage apparaît généralement automatiquement. Sinon, rendez-vous dans les paramètres d’affichage de votre ordinateur.

## Quelques astuces ...

Pour personnaliser l’organisation des écrans (définir l’écran principal, celui de gauche ou droite…), effectuer un clic droit sur le bureau puis cliquer sur « Paramètres d’affichage ».

Deux raccourcis pourront vous être utiles :

| Raccourci  | Explications |
| :----:     | :----:    |
| WIN + flèches directionnelles   | Ce raccourci vous permettra de passer votre fenêtre facilement d’un écran à l’autre et de la redimensionner pour occuper l’espace de votre choix.  |
| WIN + P | Ce raccourci permet de basculer d’un mode d’affichage à l’autre (étendre, dupliquer, 1 seul écran). |