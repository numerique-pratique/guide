---
author: Florent Déclas
title: FP 3 - Hygiène numérique
tags:
  - mise à jour
  - matériel
  - nettoyage
  - entretien
  - mots de passe
---

*Comme toute machine, votre ordinateur mérite un entretien régulier pour durer longtemps et conserver sa vélocité. Certaines habitudes sont également à adopter pour se préserver des virus et piratages (éviter les échanges de clefs USB, les mots de passe faciles…).*

## Mises à jour

Garder son ordinateur à jour est essentiel pour sa **sécurité** et ses **performances**. 
Aujourd’hui, la plupart des logiciels préviennent lorsqu’une mise à jour est disponible. Ne sous-estimez pas cette information et **prenez le temps de la faire !**
Si vous souhaitez vérifier la version de votre logiciel, il suffit, la plupart du temps, de cliquer sur « aide » puis « à propos de ce logiciel » ou « vérifier les mises à jour ».

## Nettoyer régulièrement

A l’usage, de nombreux fichiers s’accumulent et votre ordinateur ralentit. Pensez à nettoyer régulièrement.
Si vous utilisez Windows, je vous recommande [ce guide complet (Malekal)](https://www.malekal.com/guide-complet-entretien-windows/).

## Gérer ses mots de passe

**1 mot de passe par site !**
La lecture de cet excellent [guide de la SRANE de Poitiers](https://ww2.ac-poitiers.fr/srane/spip.php?article305&debut_page=2) vous aidera dans le choix de vos mots de passe. A coupler avec  cet outil en ligne.
Au sujet des **gestionnaires de mot de passe**, voir [ce dossier du site Malekal.com.](https://www.malekal.com/gestionnaires-de-mots-de-passe-le-dossier/)

## Dépoussiérer

Pour **éviter la surchauffe et une baisse de ses performances**, dépoussiérer régulièrement votre ordinateur. 
Pour cela : un petit coup de bombe dépoussiérante dans les entrailles de votre PC (éteint !) suffira.

## Supprimer le surplus

D’une manière générale, supprimez les logiciels que vous n’utilisez pas.
D’autre part, les PC sont bien souvent livrés avec des logiciels pré-installés (bloatwares). Pour faire le tri, je vous conseille cet [excellent article](https://www.malekal.com/supprimer-bloatwares-windows10/) (Malekal).


## Vérifier ses sources
Privilégiez les échanges de fichiers par internet plutôt que de brancher n’importe quelle clé USB. Le portail Apps (fiche pratique n°30) propose une application d’**échange de fichiers volumineux**. Vous pouvez également proposer un **dossier de dépôt** ([fiche pratique n°10](FP10.md)). Enfin, pensez aux transferts sans câble ([fiche pratique n°22]((FP22.md))).
Enfin, lors du **téléchargement** d’un logiciel, vérifiez toujours que vous êtes sur le **site officiel**.