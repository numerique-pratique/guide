---
author: Florent Déclas
title: FP 23 - Créer un questionnaire
tags:
  - questionnaire
  - quiz
  - QCM
  - produire
---

*Réveillez le Julien Lepers (ou Samuel Etienne pour les plus jeunes) qui sommeille en vous et voyons comment **créer des quiz en quelques clics** pour stimuler l’apprentissage de vos élèves.*

## Digistorm

* Se rendre sur [https://digistorm.app/](https://digistorm.app/)
* Cliquer sur **“Créer”**, choisir un titre et sélectionner **“Questionnaire”**.
* Vous arrivez sur la page d’administration de votre questionnaire. **Pensez à télécharger le code et mot de passe** générés pour retrouver et accéder à votre questionnaire plus tard. 
* Ajoutez vos questions, paramétrez votre quiz selon vos besoins et cliquez sur **“Enregistrer”** puis **“Lancer”**. A partir de maintenant, le lien pour les participants permet d’accéder et de répondre au questionnaire. Vous récupérez les résultats sur la page d’administration.

## Qruiz

* Se rendre sur [https://qruiz.net/](https://qruiz.net/)
* Cliquer sur **“Créer”** puis **“Créer un quiz”**.
* Saisir, à l’identique, le code affiché à l’écran.
* Créer son questionnaire puis cliquer sur **“valider”**.
* Ajuster les réglages si nécessaire puis cliquer sur **“enregistrer votre quiz”**.
* Une nouvelle page s’ouvre et indique tout ce que vous devez savoir (partage, résultats). Votre questionnaire est ainsi accessible et ouvert pour 6 mois.

## La Quizinière

Pour profiter de **fonctionnalités plus poussées** (différentes activités, suivi des élèves, catalogue collaboratif...) [La Quizinière](https://www.quiziniere.com) pourrait également vous intéresser mais, contrairement aux deux solutions précédentes, passera obligatoirement par la création d’un compte.

## Text2Quiz

[Text2quiz](https://text2quiz.vercel.app/) est un outil en ligne, **libre** et **gratuit**, qui permet de créer automatiquement un **quiz à partir d'un simple fichier texte.**