---
author: Florent Déclas
title: FP 9 - Stockage en ligne (3) - Partager ses documents
tags:
  - stockage
  - nuage
  - cloud
  - apps
  - partage
  - produire
---

*Une fois votre stockage en ligne opérationnel ([fiches pratiques 7](FP7.md) et [8](FP8.md)), le partage de vos documents s’effectuera avec une simplicité déconcertante !*

## 👉 Depuis l’interface web

Cliquer sur l’icône dédiée en face du fichier ou dossier à partager.

Cliquer sur l’icône “plus”. Un lien est généré et copié. Celui-ci permettra à n’importe qui d’y accéder. 

Après génération d’un lien de partage classique, vous aurez des points de suspension sur lesquels cliquer pour ajouter un mot de passe, limiter les autorisations, définir une date de péremption ... 

![partage1](images/FP9-1.jpg)
![partage2](images/FP9-2.jpg)


## 👉 Directement depuis votre PC (Windows)

**Prérequis :** avoir synchronisé sa machine avec le logiciel Nextcloud (fiche pratique n°8).

Ensuite, on ne peut pas faire plus simple ! **Clic droit sur le dossier ou le fichier désiré** puis “Nextcloud” et “Copier le lien public”. Vous pouvez ensuite le paramétrer selon vos désirs puis coller le lien où bon vous semble (CTRL + V) : mail à un collègue, barre d’adresse du navigateur, traitement de texte …etc 

Cela ne fonctionne que pour les fichiers et dossiers situés dans votre dossier Nextcloud. 

![partage3](images/FP9-3.jpg){ .center } 