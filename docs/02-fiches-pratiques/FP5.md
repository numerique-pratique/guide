---
author: Florent Déclas
title: FP 5 - Le vidéoprojecteur
tags:
  - vidéoprojecteur
  - entretien
  - matériel
---

## Optimiser le contenu à projeter

Lorsque vous projetez un PDF, une page internet à vos élèves ou un autre document, pensez à les passer en **plein écran** afin d’éliminer toutes les distractions et informations inutiles. Pour cela : 

* Appuyer sur **F11** (pour les navigateurs).
* Cliquer sur **"Affichage"** puis **"Plein écran"** (pour la plupart des applications).

## Entretenir le matériel

Nettoyez régulièrement **le filtre**. Un filtre encrassé peut réduire la qualité de l’image et endommager l’appareil qui risque de chauffer et diminuer sa durée de vie. Pour connaitre sa localisation, consultez la notice de l’appareil.

## Exploiter ses fonctionnalités

![Télécommande](images/telecommandevideoproj.png){ align=left } 

* **FREEZE** : pour figer l'image projetée.
En faisant cela, ce qui est fait sur l'ordinateur n'est plus visible par les élèves.
* **AV/MUTE** : pour couper la projection, sans éteindre le vidéoprojecteur. Pratique lorsque l’on a besoin d’utiliser la projection peu de temps après son interruption. Cela évite un redémarrage souvent long. Utile également pour ne pas être ébloui lorsque l’on se place devant ou lorsqu'un élève est capté par la lumière du diapo (en mode lapin devant les phares de la voiture 🐰) alors qu’on explique quelque chose !
* **WIN+P** : Ce raccourci permet de basculer d’un mode d’affichage à l’autre. Le mode “étendre” vous permettra d’agir comme si votre vidéoprojecteur était un deuxième écran. Utile pour travailler sur l’ordinateur sans déranger ce qui est projeté.
* **Intéractivité** : certains modèles sont interactifs et proposent des fonctionnalités permettant d’interagir directement avec le contenu projeté. Prenez le temps de prendre connaissance du manuel ; certaines d’entre elles pourraient vous être utiles.