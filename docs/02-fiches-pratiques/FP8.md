---
author: Florent Déclas
title: FP8 - Stockage en ligne (2) - Synchroniser toutes ses machines
tags:
  - stockage
  - nuage
  - cloud
  - apps
  - synchronisation
  - produire
---

*Vous avez un PC à l’école et un autre à la maison ? Vous souhaitez que ces deux machines soient synchronisées automatiquement (= même contenu) et disposer de vos documents même sans connexion internet ? La suite est faite pour vous …*

## 1️⃣ Télécharger Nextcloud

Télécharger et installer Nextcloud : [https://nextcloud.com/install/](https://nextcloud.com/install/) .

## 2️⃣ Configurer Nextcloud

Après l’installation et le redémarrage du système qui vous est proposé en fin de processus, Nextcloud démarre automatiquement au démarrage de Windows. Une icône est également créée sur le bureau. Lancer l’application puis cliquer sur « Se connecter »

![nextcloud1](images/FP8%201.jpg){ .center } 

Vous aurez besoin de connaitre l’**adresse du serveur** sur lequel sont stockés vos fichiers. Pour cela, ouvrir l’application nuage (fiche pratique n°7) et copier le début de l’adresse visible dans la barre d’adresse. Dans l’exemple ci-dessous, je prends « *https://nuage03.apps.education.fr* ».

![nextcloud2](images/FP8%202.jpg){ .center }

Utiliser l’adresse copiée précédemment pour compléter ce qui vous est demandé puis cliquer sur “suivant”. Nextcloud devra être autorisé à accéder à votre compte. C’est pourquoi, si tout se passe bien, votre navigateur s’ouvre. Il suffit de cliquer sur “Se connecter”, suivre le processus d’identification puis “Autoriser l’accès”.

Parfois, cela ne fonctionne pas du premier coup. Dans ce cas, répétez l'opération.

![synchro1](images/FP8%203.jpg)
![synchro2](images/FP8%204.jpg)
![synchro3](images/FP8%205.jpg)
![synchro4](images/FP8%206.jpg)

De retour sur Nextcloud, un **dossier local est créé sur votre PC**. Celui-ci contiendra **tout le contenu de votre stockage en ligne** et sera, désormais, **synchronisé automatiquement**.

Si vous préférez, vous avez la possibilité de choisir un autre chemin de dossier.
Les options à cocher vous permettent, au choix, de ne pas stocker en local, de tout synchroniser et/ou d’ajouter du contenu à synchroniser (que vous possédez déjà sur votre ordinateur). 
Si vous êtes dans le même cas que moi, c’est-à-dire que vous utilisiez déjà Nextcloud, celui-ci vous prévient que votre dossier n’est pas vide et vous avez la possibilité de conserver son contenu ou de le supprimer.
Une fois les paramètres choisis, cliquer sur “connexion” .

![nextcloud3](images/FP8%207.jpg){ .center }

La première synchronisation se lance : désormais, votre machine est synchronisée avec votre stockage en ligne. Autrement dit, **la moindre modification depuis votre ordinateur d’un fichier de votre dossier Nextcloud se répercutera sur votre stockage en ligne et vice versa, sans aucune intervention de votre part**. Vous avez accès à un panneau d’administration en cliquant sur la petite icone Nextcloud se situant dans la barre des tâches. Celle-ci vous permet également de vous assurer que tout est en ordre.

![synchro5](images/FP8%208.jpg)
![synchro6](images/FP8%209.jpg)

!!! info "Plusieurs ordinateurs ?"

    Vous pouvez **répéter l’opération avec autant de machines que vous voulez**. Ainsi, chaque terminal stockera vos fichiers en local et s’assurera, grâce une connexion internet, de toujours posséder la dernière version de vos fichiers. 