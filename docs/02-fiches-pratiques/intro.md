---
author: Florent Déclas
title: Introduction
---

# Passons à l'action

## Précisions

Abordez cette partie comme un **livre de cuisine et non une liste de compétences à maitriser**. J’imagine que vous ne connaissez  pas l’intégralité des recettes de vos livres de cuisine ! 
Généralement, nous maitrisons parfaitement celles que nous faisons fréquemment et, de temps en temps, pour épater nos invités, nous l’ouvrons pour dégoter la nouveauté qui fera mouche. 

Ici, c’est pareil ! Vous piocherez **selon vos besoins**, retiendrez les procédures auxquelles vous ferez souvent appel puis, parfois, viendrez piocher des nouveautés pour enrichir/faciliter votre enseignement ou répondre à une problématique nouvelle. 

Ce n’est **ni une course ni une compétition !**

Les solutions partagées ici résultent de choix personnels liés à mes expériences en classe ou en dehors. Libre à chacun d’en explorer ou préférer d’autres.

Je vous propose **cinq catégories de fiches** :

* **S'équiper**
* **S'organiser**
* **Produire / Gagner du temps**
* **Communiquer**
* **Aller plus loin**

Si certaines manquent de clarté ou ne fonctionnent plus, [contactez-moi](https://classeadeux.fr/nous-contacter/).

## Sommaire

### S'équiper

* [1 - Choisir et acheter un PC](FP1.md)
* [2 - Travailler avec deux écrans](FP2.md)
* [3 - Hygiène numérique](FP3.md)
* [4 - Équiper sa classe, son école](FP4.md)
* [5 - Profiter pleinement de son vidéoprojecteur](FP5.md)
* [6 - Un visualiseur ? Pour quoi faire ?](FP6.md)

### S'organiser

* [7 - Stockage en ligne (1) : ouvrir son espace](FP7.md)
* [8 - Stockage en ligne (2) : synchroniser](FP8.md)
* [9 - Stockage en ligne (3) : partager](FP9.md)
* [10 - Stockage en ligne (4) : espace de dépôt](FP10.md)
* [11 - Stockage en ligne (5) : collaborer, créer, éditer](FP11.md)
* [12 - Centraliser ses mails](FP12-1.md)
* [13 - Créer un dossier partagé sur Windows](FP12.md)
* [14 - Partager des documents avec ses élèves](FP13.md)

### Produire / Gagner du temps

* [15 - Raccourcis claviers](FP14.md)
* [16 - Caractères spéciaux et émojis](FP15.md)
* [17 - Trouver des images pour ses documents](FP16.mpd)
* [18 - Photos : flouter des visages](FP17.md)
* [19 -  Photos : retirer un fond ou un élément](FP18.md)
* [20 -  Redimensionner un lot d’images en quelques clics](FP19.md)
* [21 -  Renommer un lot de fichiers en quelques clics](FP20.md)
* [23 -  Manipuler des fichiers PDF](FP21.md)
* [23 -  Créer des questionnaires](FP22.md)
* [24 -  Générer des modèles d’écriture](FP23.md)
* [25 -  Générateurs utiles pour la classe](FP24.md)
* [26 -  Captation audio](FP25.md)
* [27 -  Montage vidéo et audio facile](FP26.md)
* [28 -  Tapuscrits faciles](FP27.md)
* [29 -  Inclusion scolaire](FP28.md)
 
### Communiquer

* [30 -  Prise de RDV automatisée avec les parents](FP29.md)
* [31 -  Organiser un évènement à plusieurs](FP31-1.md)
* [32 -  Échanger des fichiers volumineux](FP30.md)
* [33 -  Transférer du contenu entre appareils (sans câble)](FP31.md)
* [34 -  Créer un code QR](FP32.md)
* [35 -  Raccourcir un lien](FP35-1.md)
* [36 -  YouTube en classe](FP33.md)

### Aller plus loin

* [37 -  Les bases du dépannage](FP34.md)
* [38 -  Bloquer les publicités](FP35.md)
* [39 -  L’union fait la forge !](FP36.md)
* [40 -  Protéger ses données personnelles](FP37.md)
* [41 -  Quitter Windows](FP38.md)
* [42 -  Une autre idée du réseau social : Mastodon](FP39.md)
* [43 -  Les licences Creative Commons](FP40.md)