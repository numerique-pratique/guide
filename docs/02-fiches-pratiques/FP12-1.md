---
author: Florent Déclas
title: FP 12 - Centraliser ses mails
tags:
  - mail
  - gagner du temps
---

*La consultation et la gestion de vos boites mails (académique, d’école ou personnelle) peuvent être centralisées et synchronisées sur tous vos appareils (smartphone, tablette, ordinateur).*

## Comment faire ?

Synchroniser et gérer vos mails depuis tous vos appareils implique de disposer d’une application qui se substituera au site habituel de consultation de vos mails. **Une fois l’application configurée, toutes vos actions seront synchronisées** (lecture, suppression, rangement dans des dossiers...) sans avoir à vous rendre sur le site habituel de consultation de vos mails.

Plusieurs applications existent mais **la procédure de configuration sera toujours la même** : connaitre et renseigner les paramètres “IMAP” et “SMTP” de votre boite mail.

Pour connaitre ces paramètres, une recherche internet du type *“smtp imap académie...”* devrait faire l’affaire. La plupart des académies proposent des tutoriels spécifiques. Sinon, demandez à votre ERUN ou consultez [ce tableau de Julien Delmas](https://blog.juliendelmas.fr/?configurer-sa-messagerie-academique-professionnelle-sur-smartphone-tablette-ou-ordinateur). À savoir : ce tableau date de 2018.

Pour vos boites personnelles, une recherche du type *“smtp imap orange/free/bouygues/hotmail/gmail...”* devrait vous aider.

## Mozilla Thunderbird

Si vous ne savez pas vers quel logiciel vous tourner, je vous conseille [Thunderbird](https://www.thunderbird.net/fr/) car il est libre, gratuit et performant. Il fonctionne sur Windows, Linux et Android.

* **Téléchargez** et ouvrez Thunderbird.
* Cliquez sur **“Fichier”** puis **“Nouveau”** et **“Compte de messagerie”**.
* Renseignez votre **adresse mail** et le **mot de passe** associé.
* Thunderbird va essayer de détecter automatiquement les paramètres de votre messagerie. S’il échoue, ce n’est pas grave du tout. Ce sera simplement à vous de **renseigner** manuellement les paramètres **IMAP** et **SMTP** de votre messagerie. Veillez à ce que le champ “nom d’utilisateur” soit identique à l’identifiant que vous saisissez habituellement sur votre site académique (sans arobase), choisissez **“mot de passe normal”** puis cliquez sur **“terminé”**.

Une fois votre compte configuré sur ordinateur, vous pourrez le **récupérer facilement sur votre smartphone** à l’aide d’un simple **code QR**. 

* Cliquez sur **“outils”** puis **“exporter une version mobile”**.
* Faites vos choix et cliquez sur **“exporter”**.
* Sur votre **mobile**, ouvrez **Thunderbird** et cliquez sur **“importer les paramètres”**. 
* **Scannez** le code QR.