---
author: Florent Déclas
title: FP 4 - Équiper sa classe
tags:
  - matériel
  - classe
  - équipement
---

## Avant tout ... 

**L’équipement d’une classe ou d’une école doit répondre à un besoin et non provenir d’une lubie de supérieur hiérarchique ou d’élu local.** Après la photo dans le journal, rappelez-vous que c’est votre quotidien qui sera impacté et non celui de la personne uniquement présente pour le flash de l’appareil photo.
Pour tout conseil, je rappelle que votre ERUN de circonscription (Enseignant Référent pour les Usages du Numérique) doit être votre interlocuteur privilégié.

## 1️⃣ Evaluer les besoins

Identifiez clairement ce que vous souhaitez accomplir avec les outils numériques : différencier, améliorer la collaboration, enrichir vos cours avec des ressources interactives, les rendre plus ludiques, travailler la programmation ...

## 2️⃣ Choisir son matériel : tablettes ou PCs ?

Comme toujours, la réponse à cette question dépend de vos besoins.
En terme d’applications, la généralisation des outils directement accessibles depuis un navigateur web tend à rapprocher les deux machines.
Pour moi, la différence réside essentiellement dans les modalités d’utilisation. Une tablette sera souvent plus intuitives pour les élèves, notamment les plus jeunes et idéale pour des activités courtes et ludiques. Le PC sera plus complexe à prendre en main mais offre plus de possibilités en termes de multitâche et de projets plus complexes (recherche internet, traitement de texte, programmation, création de présentation …). Ainsi, dans notre RPI, nous avons fait le choix d’équiper les élèves de cycles 1 et 2 en tablettes et ceux de cycle 3 en PC portables et nous ne regrettons rien (Edith, si tu nous regardes 😉).


## 3️⃣ Choisir son matériel : le reste

Je ne vais pas vous mentir. Je n’ai pas le recul nécessaire pour vous orienter concernant les vidéoprojecteurs classiques, interactifs, les écrans interactifs, les TBI, TNI et compagnie ... 
La règle d’or reste la suivante : le matériel choisi doit vous rendre plus efficace et se montrer bénéfique pour votre pratique pédagogique. Sinon, fuyez !

## 4️⃣ Gérer le matériel

Prévoyez des sessions régulières de maintenance (mise à jour, nettoyage des fichiers, ...) et établissez des règles claires pour l’usage des appareils si ceux-ci sont partagés.

## 5️⃣ Connexion Internet

De plus en plus d’outils sont utilisables en ligne. Cela permet d’éviter de passer par la case installation mais oblige à disposer d’une connexion internet solide. Ne négligez donc pas ce point.

## 6️⃣ Sélection de logiciels et applications

Pour cela, je vous invite à continuer la lecture de ce site. Vous ne devriez pas repartir bredouille,  entre la partie 1.c. “Garnir sa boite à outils numériques”, les fiches pratiques et la 3e partie “Naviguons ensemble”.

## 👉 À noter
 Il existe un [référentiel concernant l'équipement numérique de base à l'école publié sur Eduscol](https://eduscol.education.fr/1066/socles-d-equipement-numerique-definis-en-comite-des-partenaires).