---
author: Florent Déclas
title: FP 1 - Choisir et acheter son ordinateur
tags:
  - équipement
  - matériel
  - ordinateur
  - PC
---

## 1️⃣ Comprendre les composants

### Processeur 
Le cerveau de votre ordinateur. Plus il est puissant, plus il permet à l’ordinateur d’accomplir ses tâches rapidement. 
Lorsque vous lirez les caractéristiques proposées par les vendeurs, vous trouverez généralement sa marque (AMD / Intel) suivi du nom de sa gamme (Athlon, Ryzen pour AMD et Pentium, Celeron, i…, m…, n... pour Intel).
Une donnée chiffrée (en GHz) est également indiquée. Sa seule valeur ne suffit pas à comparer deux PC. Il faut également comparer leur gamme.

### Mémoire vive (ou RAM)
La mémoire de travail de votre ordinateur. Elle sert de stockage temporaire pour le traitement des tâches du processeur. Elle est indiquée en Go. Plus il y en a, mieux c’est. Cependant, inutile de disposer de 32 Go de RAM pour consulter ses mails.

### Disque dur
La mémoire à long terme de votre machine. HDD signifie « Hard Drive Disk ». Ce système est en voie de disparition car plutôt lent (à cause de son fonctionnement mécanique). Les HDD sont désormais remplacés par des SSD (Solid State Drive) beaucoup plus rapides (grâce à un fonctionnement électronique). Leur capacité de stockage est exprimée en Go (Gigaoctets) ou To (Teraoctets). Plus le nombre est élevé, plus vous disposerez de place pour le stockage de vos fichiers. A savoir : 1 To = 1024 Go.

### Carte graphique
Essentiellement pour les jeux, le traitement de l’image ou les montages vidéos. Dans les ordinateurs portables, le processeur graphique est souvent intégré au processeur principal (intel HD, Iris, Vega …). Parfois une carte dédiée est installée (de marque AMD ou NVIDIA).

### Lecteur optique
La présence d’un lecteur optique (lecteur CD/DVD) est de plus en plus rare sur les ordinateurs portables. Des solutions externes existent (branchement par USB) et se situent entre 15 et 50 €.

### Connectique
Voici quelques références à connaitre : [^1]
![Connecteurs](images/Connectique.jpg){ .center } 


## 2️⃣ Faire son choix

*Pas de panique ! Aujourd’hui, la plupart des machines conviennent à l’utilisation de l’enseignant “lambda” qui surfe sur le net, utilise une suite bureautique et quelques applications pédagogiques… Cependant, savoir ce que l’on achète est toujours plus rassurant. Voici quelques éléments.*

### Ordinateur portable ou fixe ?
Si votre portable reste sur le même bureau à la maison, pensez sérieusement au fixe. Aujourd’hui, **les tours d’ordinateurs sont beaucoup plus petites que dans vos souvenirs** (moins de 20 cm pour certaines) et, pour un prix identique, proposent souvent de meilleures performances. Autres avantages : pas de problème à long terme de batterie et la possibilité d’avoir un écran plus grand pour plus de confort.

### Composants
- **Processeur** : pour être serein, privilégiez la gamme i3/5/7 chez Intel ou Ryzen 3/5/7 chez AMD.
- **Mémoire vive** (ou RAM) : 8Go minimum.
- **Carte graphique** : inutile de disposer d’une carte graphique dédiée si vous restez sur des tâches basiques même si vous faites de temps en temps des petits montages pour votre classe.
- **Connectique** : pensez à regarder le nombre de ports USB selon votre utilisation ainsi que la sortie vidéo (HDMI / VGA) dans le cas où vous auriez besoin de connecter votre PC à un vidéoprojecteur. Quoiqu’il arrive, de nombreux adaptateurs existent dans le commerce et permettent bien souvent de s’en sortir.
- **Système d’exploitation** : concernant Windows, méfiez vous des versions « S » qui sont limitées. Si vous êtes en quête de liberté et de nouvelles contrées, intéressez-vous de plus près à Linux (fiche technique n°37).

### Où acheter ?
De nombreuses entreprises renouvellent leur parc informatique régulièrement, laissant des tonnes de machines fonctionnelles sur la touche. Heureusement, certains professionnels se sont spécialisés dans le **reconditionnement** de ce matériel pour le revendre à des prix attractifs tout en proposant une **garantie**. Souvent, ces ordinateurs sont issus des gammes professionnelles des constructeurs. Ils n’ont pas l’effet « wahou » des gammes à destination des particuliers mais sont souvent plus facilement réparables et durables.

Une de ces entreprises, découverte grâce à mon ERUN que je remercie vivement, est « DIGITALEA ». Elle propose son matériel sur deux sites internet : [ITjustGood](https://www.itjustgood.com/) (anciennement Tradediscount) et [Laptop service](https://www.laptopservice.fr/).
Je vous les conseille sans aucun intérêt personnel. Après plusieurs commandes chez eux, j’ai entièrement confiance. Détail important pour les écoles : le paiement par mandat administratif et propose des lots d’ordinateurs. Équiper une classe devient alors très accessible ! 

Autre site découvert récemment : une coopérative sociale et environnementale : [https://ateliers-du-bocage.fr/](https://ateliers-du-bocage.fr/) qui propose des prix attractifs ainsi que du matériel tournant sur PrimTux (un système d’exploitation libre et gratuit prévu pour les écoles).

Ne négligez pas non plus vos entreprises informatiques locales qui peuvent également proposer du reconditionné. 

[^1]: Illustrations : [@Irasutoya](https://www.irasutoya.com/)