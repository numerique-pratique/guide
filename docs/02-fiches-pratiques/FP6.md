---
author: Florent Déclas
title: FP 6 - Le visualiseur 
tags:
  - visualiseur
  - matériel
---

## Qu'est-ce que c'est ?

Un visualiseur est tout simplement une **caméra placée sur un pied** permettant de filmer et de projeter (via un vidéoprojecteur) ce que vous avez à proximité.
Selon les modèles, un **logiciel** proposé par la marque apportera des **fonctionnalités supplémentaires** telles que l’enregistrement vidéo, les annotations, le zoom, la capture en direct, le scan, …

## Exemples d'utilisation

- Projeter une **production d’élève** pour travailler collectivement dessus (correction, analyse …),
- **Manipuler de petits objets** pour que tout le monde voit bien (matériel de numération, glisse-nombre, expliquer la règle d’un jeu…).
- Montrer en direct des **techniques de tracé** en géométrie, les étapes d’un **travail artistique** ...
- Montrer des **photos**, un **document précis** d’une page d’un livre, des **objets** sans avoir à les faire passer de main en main (notamment lorsque les élèves souhaitent partager un objet, livre ou dessin de la maison).

!!! info "Quel modèle ?"

    Concernant le choix d’un modèle, je vous redirige vers votre ERUN de circonscription car je n’ai, ni le recul, ni l’expérience suffisante pour juger les différents modèles.
    Je peux tout de même vous donner quelques références à étudier (dans un ordre croissant de prix) : **HUE HD, IPEVO V4K, Lumens DC125, AverVision U50, Optoma DC450 **(liste non exhaustive !).
    Certaines de ces marques proposent d’autres références à étudier selon vos besoins.