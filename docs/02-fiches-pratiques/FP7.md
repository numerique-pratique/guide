---
author: Florent Déclas
title: FP 7 - Stockage en ligne (1) - Ouvrir son espace de stockage
tags:
  - stockage
  - nuage
  - cloud
  - apps
  - produire
---

# Ouvrir son stockage en ligne

Pour profiter des **100 Go de stockage en ligne** avec vos **identifiants académiques** mis à disposition par le ministère de l’Éducation Nationale, voici la marche à suivre :

## 1️⃣ Connexion
Se rendre sur :  [https://portail.apps.education.fr/signin](https://portail.apps.education.fr/signin)
Cliquer sur **“Se connecter”.**

![Connexion](images/FP7%201.jpg){ .center } 

## 2️⃣ Authentification

Cliquer sur le bouton **“ Authentification Éducation Nationale”**, choisir son académie puis saisir ses identifiants académiques.

![authentification1](images/FP7%202.jpg)
![authentification2](images/FP7%203.jpg)
![authentification3](images/FP7%204.jpg)

## 3️⃣ Accéder au stockage

Une fois identifié, **accéder à l’outil de stockage en ligne** (Nuage).

![Connexion](images/FP7%205.jpg){ .center } 

## 4️⃣ C'est fait ! 

![panneaunuage](images/FP7%206.jpg){ .center } 

Ceci est l’interface « internet » de votre cloud. Depuis cette page, vous pouvez déjà faire énormément de choses : déposer des documents, les organiser en dossiers, partager, éditer, créer … A ce stade, tout ce que vous ferez sera stocké en ligne seulement. 
Si vous souhaitez synchroniser vos machines, rdv à la fiche pratique suivante ([n°8](FP8.md)).