---
author: Florent Déclas
title: Introduction
---

# Naviguons ensemble

## Précisions

Contrairement à un **ramasseur de champignons** qui ne vous dévoilera certainement pas ses plus beaux coins par peur de ne plus rien trouver lors de sa prochaine promenade, je m’apprête à lister mes plus beaux coins du net sans aucune appréhension car c’est bien cela la force d’un **outil numérique** (encore plus lorsqu’il est libre) : pouvoir être **partagé et utile au plus grand nombre**.

Je vous propose deux parties : 

* **La première**, composée de fiches individuelles me permettant d’apporter des précisions ou de mettre en valeur certains outils (choix totalement subjectif !).
* **La seconde**, sous la forme d’une liste de sites que je vous laisse libre de fouiller, visiter, tester selon vos besoins.

Dans les deux cas, les sites seront classés par **ordre alphabétique** (j’ai triché pour Apps Education mais je voulais absolument le mettre en 1er ! Faute avouée, à moitié pardonnée 😉).

Enfin, je précise que les sites à destination des **maternelles** seront fortement **sous représentés** car j’enseigne actuellement en CE2, CM1 et CM2.

Bonne navigation !

## Sommaire

### Fiches individuelles

1. [Apps](S1.md)
2. [Anton](S2.md)
3. [Arcademics](S3.md)
4. [Banque de problèmes](S4.md)
5. [Bibliopocket](S5.md)
6. [Cap École Inclusive](S6.md)
7. [Classe de Florent](S7.md)
8. [Educajou](S8.md)
9. [ICT Games](S9.md)
10. [Jepeuxpasjaimaths](S10.md)
11. [La classe du Lama](S11.md)
12. [La Digitale](S12.md)
13. [Mathigon](S13.md)
14. [Mathix](S14.md)
15. [Maths-en-un](S15.md)
16. [Mathsmentales](S16.md)
17. [MiCetF](S17.md)
18. [MonEcole](S18.md)
19. [Multimaths](S19.md)
20. [OpenBoard](S20.md)
21. [Phet](S21.md)
22. [Pixeludo](S21.md)
23. [LaForgeEdu](S23.md)
24. [Tatitotu](S24.md)

### En vrac

Un très beau tableau, à l'ancienne, avec **50 autres sites** [en cliquant ici](vrac.md).
