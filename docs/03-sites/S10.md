---
author: Florent Déclas
title: S10 - Je peux pas j'ai maths 
---

## En bref

Un site qui se concentre sur le **calcul mental** et qui le fait extrêmement bien. Simple, rapide, sans publicité et efficace ! Idéal pour un **usage individuel** mais également en **classe entière**.

<center>[https://www.jepeuxpasjaimaths.fr/](https://www.jepeuxpasjaimaths.fr/){ .md-button target="_blank" rel="noopener" }</center>

## En détails

En arrivant sur le site, deux possibilités : 

* **Mode libre**  : une utilisation directe et libre du site.
* **S’identifier** : pour un suivi des résultats et d’autres fonctionnalités (scores, records, séries d’exercices ...).

Les notions sont triées par cycle puis par thèmes.

Après sélection d’un thème, plusieurs paramètres sont personnalisables. Deux modes sont proposés :

* **Élève** : pour un entrainement individuel.
* **Professeur** : pour une projection collective.

![jppjm](images/S10.jpg){ .center } 