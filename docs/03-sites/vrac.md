---
author: Florent Déclas
title: Sites en vrac 
---

## Précisions

N’allez pas croire que les sites à venir ont moins de valeur ! Pour reprendre la métaphore des coins à champignons utilisée en introduction, disons que, pour la première partie, je les ai récoltés, nettoyés et classés pour vous. Désormais, à vous de parcourir le coin et de cueillir ceux qui vous plaisent.

**Certains sites sont payants.**

## Les sites 

| Nom de l'outil                  | Site Web                                              | Description                                                                                       |
|----------------------------------|-------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| **Adaptativ Maths**              | [adaptivmath.fr](https://www.adaptivmath.fr/)         | Outil pédagogique pour les mathématiques au cycle 2.                                               |
| **Applivox**                     | [applivoix.fr](https://applivoix.fr/)                 | Outil d’entraînement pour le chant et partenaire de chauffe.                                       |
| **Apps4Edu**              | [apps4edu.fr](https://apps4edu.fr)         | Applications pédagogiques ludiques et engageantes, créées par un enseignant pour les enseignants et leurs élèves.                                               |
| **Archipel**                     | [archipel.education](https://archipel.education/ecoles/) | Jeu vidéo pédagogique autour des mathématiques pour le cycle 3.                                    |
| **ASH21**                        | [ash21.alwaysdata.net](https://ash21.alwaysdata.net/) | Annuaire de logiciels, applications et extensions au service de l’inclusion scolaire.              |
| **Atelier des problèmes**        | [atelier.appenvie.fr](https://atelier.appenvie.fr/)    | Plateforme pour s'entraîner en résolution de problèmes du CP au CM2.                               |
| **Boukili**                      | [boukili.ca](https://app.boukili.ca/)                 | Livres à lire ou écouter en ligne avec suivi des progrès des élèves.                               |
| **Calcul@TICE**                  | [calculatice.ac-lille.fr](https://calculatice.ac-lille.fr/) | Site d’entraînement des élèves au calcul mental avec exercices ou parcours personnalisés.         |
| **Captain Kelly**                | [belin-education.com](https://www.belin-education.com/captain-kelly-0) | Assistant vocal pour l’apprentissage de l’anglais à l’école élémentaire.                            |
| **Chanson d’écoles**             | [chansonsdecole.eklablog.com](http://chansonsdecole.eklablog.com/) | Chansons à télécharger gratuitement avec textes, instrus et accords.                               |
| **Classe +**                     | [laclasseplus.fr](http://www.laclasseplus.fr/)         | Des vidéos et des applications pour réviser.                                                       |
| **Coloring Squared**             | [coloringsquared.com](https://coloringsquared.com/)   | Le temple des coloriages magiques façon “Pixel Art”.                                               |
| **Dans ta tête**                 | [danstatete.cool](https://danstatete.cool/)           | Fiches et vidéos pour apprendre à apprendre !                                                      |
| **Dictées négociées**            | [desmaths.fr](https://www.desmaths.fr/dicteenegociee/) | Outil pour animer vos dictées négociées.                                                            |
| **Dictionnaire APIceras**        | [dico.apiceras.ch](https://dico.apiceras.ch/)         | Dictionnaire illustré et inclusif en ligne pour favoriser la compréhension et l’expression écrite. |
| **Domino**                       | [domino.education](https://domino.education/)         | Solution numérique complète pour le cycle 2.                                                       |
| **Eyseette**                     | [eyssette.forge.apps.education.fr](https://eyssette.forge.apps.education.fr/) | Compilation des sites et outils proposés par Cédric Eyssette, professeur de philosophie.          |
| **Forge**                        | [docs.forge.apps.education.fr](https://docs.forge.apps.education.fr/) | Lieu de fédération et de partage des créateurs de logiciels et ressources éducatives libres.        |
| **Foxar**                        | [foxar.fr](https://foxar.fr/)                         | Bibliothèque de maquettes pédagogiques.                                                             |
| **Gcompris**                     | [gcompris.net](https://www.gcompris.net/index-fr.html) | Logiciel éducatif avec une centaine d’activités pour enfants.                                      |
| **Géomaitre**                    | [geomaitre.alwaysdata.net](https://geomaitre.alwaysdata.net/menu.php) | Application de géométrie en ligne destinée aux élèves dyspraxiques.                                |
| **How Many**                     | [howmanyjeu.fr](https://howmanyjeu.fr/)               | Application pour apprendre et s’entraîner à calculer via des dessins.                              |
| **Iaconelli**                    | [lmdbt.forge.apps.education.fr](https://lmdbt.forge.apps.education.fr/) | Compilation des applications développées par Cyril Iaconelli, professeur de mathématiques.        |
| **Lalilo**                       | [lalilo.com](https://lalilo.com/)                     | Application pour l’enseignement de la lecture, adaptative au niveau des élèves.                    |
| **Les fondamentaux**             | [lesfondamentaux.reseau-canope.fr](https://lesfondamentaux.reseau-canope.fr/) | 600 films d’animation pour apprendre avec fiches d'accompagnement.                                  |
| **LireCouleur**                  | [lirecouleur.forge.apps.education.fr](https://lirecouleur.forge.apps.education.fr/weblc6/#) | Outil pour faciliter le décodage des mots en utilisant la lecture en couleur.                      |
| **MarkLab**            | [marklab.forge.apps.education.fr](https://marklab.forge.apps.education.fr) | Une série d'applications qui utilisent le Markdown (syntaxe de mise en forme très accessible) pour produire des cartes mentales, des quiz, des flipbooks…                                                  |
| **Logiciel éducatif**            | [logicieleducatif.fr](https://www.logicieleducatif.fr/) | La référence des jeux éducatifs gratuits en ligne.                                                  |
| **Materalbum**                   | [materalbum.free.fr](http://materalbum.free.fr/)      | Catalogue d’activités à télécharger autour d’albums à exploiter en maternelle.                      |
| **Mathia**                       | [mathia.education](https://mathia.education/)         | Parcours d’activités sur mesure pour les mathématiques au cycle 2.                                 |
| **Maths Learning Center**        | [mathlearningcenter.org](https://www.mathlearningcenter.org/apps) | Des applications pour modéliser et manipuler les concepts mathématiques.                           |
| **MathsBot**                     | [mathsbot.com](https://mathsbot.com/manipulativeMenu) | Applications pour modéliser et manipuler les concepts mathématiques.                               |
| **MonAideNumérique**                     | [monaidenumerique.education.gouv.fr](https://monaidenumerique.education.gouv.fr) | Tutoriels, documentations et FAQ pour vous accompagner dans l'usage de vos services numériques.                               |
| **MusiquePrim**                         | [reseau-canope.fr/musique-prim.html](https://www.reseau-canope.fr/musique-prim.html)             | Répertoire d’œuvres à écouter ou à chanter (pistes audio, partitions, paroles...).|
| **Navi**                         | [navi.education](https://navi.education/)             | Assistant intelligent pour la remédiation et la mémorisation des compétences de lecture et d’écriture.|
| **OctoStudio**                   | [octostudio.org](https://octostudio.org/fr/)          | Application de code gratuite pour créer n’importe où, n’importe quand.                            |
| **OpenBoard**                   | [openboard.ch/](https://openboard.ch/)          |  Logiciel d’enseignement interactif pour TNI open source conçu pour être utilisé dans les écoles.                             |
| **Orthophore**                   | [orthophore.fr](https://www.orthophore.fr/)           | Propose des dictées hebdomadaires du CE1 au CM2 sous forme de textes à trous.                       |
| **Pikado**                   | [pikado.education](https://pikado.education)           | Collection d’outils pédagogiques gratuits sous la forme d’applications web.                      |
| **Pit et Pit**                   | [pitetpit-free.fr](https://pitetpit-free.fr/)         | Applications pour apprendre en s'amusant du CP au CM2.                                               |
| **Plume**                        | [plume-app.co](https://plume-app.co/)                 | Activités d’écriture et un assistant rédactionnel à partir de 7 ans.                               |
| **PowerZ**                       | [powerz.tech](https://powerz.tech/fr-fr/)             | Jeu vidéo éducatif qui s’adapte au niveau de l’enfant.                                               |
| **PragmaTICE**                   | [pragmatice.net](https://pragmatice.net/lesite/)       | Association avec des ressources et activités interactives pour TNI.                                |
| **Primschool**                   | [primschool.com](https://primschool.com/)             | Application pour le suivi des élèves et la gestion des préparations.                               |
| **PrimTux**                       | [primtux.fr](https://primtux.fr/)                     | Système d’exploitation Linux gratuit dédié à l’apprentissage.                                      |
| **Professeurphifix**             | [professeurphifix.net](https://www.professeurphifix.net) | Fiches, vidéos et exercices en ligne pour s’entraîner du CP au CM2.                                 |
| **QCM cam**                      | [qcmcam.net](https://qcmcam.net/)                     | Outil pour sonder vos élèves avec une webcam ou smartphone.                                         |
| **Ressources école inclusive**   | [ressources-ecole-inclusive.org](https://ressources-ecole-inclusive.org/) | 400 posters, outils et formations pour l’école inclusive.                                           |
| **Scratch Jr**                   | [scratch.mit.edu](https://scratch.mit.edu/)           | Logiciel de codage pour enfants via une interface simple.                                           |
| **Smart Enseigno**               | [smartenseigno.fr](https://www.smartenseigno.fr/)     | Plateforme d’enseignement des mathématiques au cycle 2.                                            |
| **Tacit**                         | [tacit.univ-rennes2.fr](https://tacit.univ-rennes2.fr/) | Plateforme d’évaluation et de remédiation aux difficultés de lecture.                              |
| **Teachapp**                     | [teachapp.fr](https://www.teachapp.fr/)               | Outils numériques permettant d'améliorer le quotidien des enseignants.                             |
| **Templatemaker**                | [templatemaker.nl](https://www.templatemaker.nl/fr/)  | Patrons de boîtes à paramétrer et imprimer.                                                          |
| **ThatQuiz**                     | [thatquiz.org](https://www.thatquiz.org/)             | Générer facilement des quiz en ligne.                                                              |
| **ToyTheater**                     | [toytheater.com](https://toytheater.com)             | Jeux en ligne et outils de modélisation (en anglais).                                                              |
| **Tramopé**                      | [ash21.alwaysdata.net](https://ash21.alwaysdata.net/tramope/) | Générer une trame d’opération et la corriger.                                                     |
| **51 activités de création écrite** | [christophe-rhein.canoprof.fr](https://christophe-rhein.canoprof.fr/eleve/Mes_cours.../Cycle_3/51_activit%C3%A9s_de_cr%C3%A9ation_%C3%A9crite_cycle3/activities/act_creation_acrostiches.xhtml) | Activités de création écrite pour le cycle 3.                                                       |


