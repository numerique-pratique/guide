# Le numérique pratique : pour les profs des écoles

![Bannière](docs/images/Num-pratique.png){ align=center } 

## Objectifs

* Transposer sous la forme d'un mini-site le guide ["Le numérique pratique : pour les profs des écoles"](https://classeadeux.fr/le-numerique-pratique/) ;
* Faciliter son partage et la possibilité de l'adapter grâce aux fonctionnalités de LaForgeEdu.

## Licence

[CC-BY](https://creativecommons.org/licenses/by/4.0/deed.fr)

## Accès au site

https://numerique-pratique.forge.apps.education.fr/guide/